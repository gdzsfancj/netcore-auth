# .NET Core 中的鉴权授权正确方式

原文地址：
[.NET Core 中的鉴权授权正确方式]( https://mp.weixin.qq.com/s/2z1A6O0OufYU7EfH5N121Q)

## 简介

前后端分离的站点一般都会用jwt或IdentityServer4之类的生成token的方式进行登录鉴权。这里要说的是小项目没有做前后端分离的时站点登录授权的正确方式。

## 传统授权方式

传统授权方式用session或cookies来完成，将用户信息保存在session或cookies中

1. 在请求某个Action之前去做校验，验证当前操作者是否登录过，登录过就有权限

2. 如果没有权限就跳转到登录页中去

3. 传统登录授权用的AOP-Filter：ActionFilter。

### 具体实现

项目：Auth.Classic

### 认证Authentication

1. 创建``CurrentUser``类

2. 创建登录控制器``AccountController``

3. ``Login``验证用户成功后，通过HttpContext将包含用户信息的Cookie传回浏览器，或使用session保存用户信息

   ```c#
   // AccountController
   // 验证用户成功后
   CurrentUser currentUser = new CurrentUser { Id = 123, Name = "张三", Account = userName };
   string userJson = JsonSerializer.Serialize(currentUser);
   
   // 使用cookie 关键
   HttpContext.Response.Cookies.Append("CurrentUser", userJson, new CookieOptions
                       {
                           Expires = DateTime.Now.AddMinutes(30)
                       });
   
   // 使用session
   HttpContext.Session.SetString("CurrentUser", userJson);
   ```

   > 注意：使用session前在先在startup配置相应服务和中间件

   ```c#
   // Startup.cs
   public void ConfigureServices(IServiceCollection services)
   {
       services.AddSession();
       services.AddControllersWithViews();
   }
   
   public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
   {
       app.UseSession();
   	app.UseAuthorization();
   }
   ```


4. ``HomeController``中获取相应的cookie或session

   ```C#
   // HomeController
   string userString = string.Empty;
   
   if (loginType == "cookie")
   {
       HttpContext.Request.Cookies.TryGetValue("CurrentUser", out userString);
   }
   else
   {
       userString = HttpContext.Session.GetString("CurrentUser");
   }
   ```

### 鉴（授）权Authorize

鉴权通过过滤器实现

1. 新建过滤器``MyActionAuthrizaFilterAttribute ``，继承:``Attribute``，实现`` IActionFilter``

   ```c#
   public class MyActionAuthrizaFilterAttribute : Attribute, IActionFilter
   { ... }
   ```

2. 实现``IActionFilter``的``OnActionExecuting``方法，另一方法暂不实现``OnActionExecuted``

   ```c#
   public void OnActionExecuting(ActionExecutingContext context)
   { ... }
   ```

3. 从``OnActionExecuting``的ActionExecutingContext参数中获取cookie或seesion，从而完成鉴权。``ActionExecutingContext``还有很多有用的属性，有空可研究一下。

   ```C#
   // MyActionAuthrizaFilterAttribute.cs
   public void OnActionExecuting(ActionExecutingContext context)
   {
       Console.WriteLine("开始验证权限...");
   
       if (!context.HttpContext.Request.Cookies.TryGetValue("LoginType", out string loginType))
       {
           context.Result = new JsonResult(new
                                           {
                                               Success = false,
                                               Message = "没有权限"
                                           });
           context.Result = new RedirectResult("/Account/Login");
           return;
       }
   
       string sUser;
       if (loginType == "cookie")
       {
           // 从cookie中获取用户信息
           context.HttpContext.Request.Cookies.TryGetValue("CurrentUser", out sUser);
       }
       else
       {
           // 从session中获取用户信息
           sUser = context.HttpContext.Session.GetString("CurrentUser");
       }
   
       // 获取请求头
       string header = context.HttpContext.Request.Headers["X-Requested-With"];
       // 是否ajax请求
       bool isAjaxRequest = "XMLHttpRequest".Equals(header);
   
       // 这里简单地判断有用户信息就有权访问
       if (string.IsNullOrEmpty(sUser))
       {
           Console.WriteLine("没有权限...");
           if (isAjaxRequest)
           {
               context.Result = new JsonResult(new
                                               {
                                                   Success = false,
                                                   Message = "没有权限"
                                               });
           }
           context.Result = new RedirectResult("/Account/Login");
           return;
       }
       Console.WriteLine("权限验证成功...");
   }
   ```

## .NET5中正确的鉴权方式

<img src="E:\projects\netcore-auth\640.webp" alt="640"  />

> .NET5的filter顺序，传统的授权方式在``Action filter(before)``完成
> 正确的鉴权应该是在``Authorization filter``中做，``Authorization filter``是.NET5里面专门做鉴权授权用的。

### 基本认证授权

1. 在``Startup.Configure``中的``app.UseRouting``和``app.UseEndpoints``之间加入认证授权中间件

   ```c#
   // Startup.cs
   ...
   app.UseRouting();
   
   app.UseAuthentication(); // 认证=>是否登录
   app.UseAuthorization(); // 授（鉴）权=>能否访问资源
   
   app.UseEndpoints(endpoints =>
                    {
                        ...
                    });
   ```

2. ``ConfigureServices``中启用服务

   ```c#
   // Startup.cs
   using Microsoft.AspNetCore.Http;
   using Microsoft.AspNetCore.Authentication.Cookies;
   ...
   services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
       .AddCookie(option =>
                  {
                      //没登录跳到这个路径
                      option.LoginPath = new PathString("/Account/Login");
                  });
   ```

3. 在需要登录认证的控制器或方法上增加``[Authorize]``，允许匿名增加``[AllowAnonymous]``，当访问需要登录的页面时，如果用户未登录会自动跳转到``/Account/Login``

   ```c#
   using Microsoft.AspNetCore.Authorization;
   
   [Authorize]
   public class HomeController : Controller
   {
       ...
       
        [AllowAnonymous]
        public IActionResult Privacy()
       { ... }
   }
   ```

4. 创建登录控制器``AccountController``和``Login``方法，重点是使用Claim保存用户信息，包括角色信息

   ```c#
   // AccountController
   public class AccountController : Controller
   {
       public IActionResult Login()
       {
           return View();
       }
       
       [HttpPost]
       public IActionResult Login(IFormCollection formData)
       {
           ...
          	// 用Claim保存用户信息
   		List<Claim> claims = new List<Claim>()
       	{
           	new Claim(ClaimTypes.Name,"测试账号"),
           	new Claim("id","1"),
           	new Claim("account",userName)
           	// 可以继续增加
       	};
           // 再增加角色
           foreach (var role in roleList)
           {
               claims.Add(new Claim(ClaimTypes.Role, role));
           }
           ...
       }
   }
   ```

### 基于角色的授权

1. 认证成功再加上角色的授权可以指定无权限时的跳转页，``ConfigureServices``中增加无权限页

    ```c#
    // Startup
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllersWithViews();
        //services.AddSession(); 传统鉴权
    
        services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options => {
                options.LoginPath = new PathString("/Account/Login");//没登录跳到这个路径
                options.AccessDeniedPath = new PathString("/Account/AccessDenied");//没权限跳到这个路径
            });
    }
    ```

2. 在控制器或方法上指定角色

   ```c#
   // HomeController
   
   // 三种授权组合
   
   // 1对1 
   [Authorize(Roles ="Admin")]
   public IActionResult roleData1() 
   {
       return Content("Admin可访问");
   }
   // 多角色
   [Authorize(Roles = "Admin,User")]
   public IActionResult RoleData2()
   {
       return Content("Admin或User可访问");
   }
   
   // 同时符合
   [Authorize(Roles = "Admin")]
   [Authorize(Roles = "Test")]
   public IActionResult RoleData3()
   {
       return Content("同时拥有Admin和Test可访问");
   }
   ```

### 自定义策略授权

基于角色的授权缺点是角色写死在代码中(``[Authorize(Roles = "Admin")]``)，修改只能改原代码，实际项目中权限都是根据配置修改的，所以要用到自定义策略

1. 增加``CustomAuthorizationRequirement.cs``，并实现``IAuthorizationRequirement``，``IAuthorizationRequirement``没有任何方法

   ```C#
   // CustomAuthorizationRequirement.cs
   public class CustomAuthorizationRequirement : IAuthorizationRequirement
   {
       public string Name { get; set; }
   
       public CustomAuthorizationRequirement(string policyname)
       {
           Name = policyname;
       }
   }
   ```

2. 增加``CustomAuthorizationHandler.cs``，并继承``AuthorizationHandler<CustomAuthorizationRequirement>``泛型抽象类

   ``` c#
   
   ```

3. 使自定义授权策略生效，在Startup中注册

   ```c#
   ```

   

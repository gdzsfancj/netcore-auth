﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using _NET5中的鉴权授权正确方式.Models;
using Microsoft.AspNetCore.Authorization;

namespace _NET5中的鉴权授权正确方式.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Authorize(Roles = "Admin")]
        public IActionResult RoleData1()
        {
            return Content("Admin可访问");
        }

        [Authorize(Roles = "Admin,User")]
        public IActionResult RoleData2()
        {
            return Content("Admin或User可访问");
        }

        [Authorize(Roles = "Admin")]
        [Authorize(Roles = "Test")]
        public IActionResult RoleData3()
        {
            return Content("同时拥有Admin和Test可访问");
        }

        [Authorize(policy: "customPolicy")]
        public IActionResult RoleData4()
        {
            return Content("自定义授权策略");
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _NET5中的鉴权授权正确方式.Extensions
{
    public class CustomAuthorizationRequirement : IAuthorizationRequirement
    {
        public string Name { get; set; }

        public CustomAuthorizationRequirement(string policyname)
        {
            Name = policyname;
        }
    }
}

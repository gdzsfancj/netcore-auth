﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _NET5中的鉴权授权正确方式.Extensions
{
    public class CustomAuthorizationHandler : AuthorizationHandler<CustomAuthorizationRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CustomAuthorizationRequirement requirement)
        {
            bool flag = false;

            if (requirement.Name == "Policy01")
            {
                Console.WriteLine("进入自定义策略授权01...");
                ///策略1的逻辑
            }

            if (requirement.Name == "Policy02")
            {
                Console.WriteLine("进入自定义策略授权02...");
                ///策略2的逻辑
            }

            if (flag)
            {
                context.Succeed(requirement); //验证通过了
            }

            return Task.CompletedTask; //验证不同过
        }
    }
}

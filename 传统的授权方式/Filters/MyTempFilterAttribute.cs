﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Classic.Filters
{
    public class MyTempFilterAttribute : Attribute, IActionFilter
    {
        void IActionFilter.OnActionExecuted(ActionExecutedContext context)
        {
            //throw new NotImplementedException();
        }

        void IActionFilter.OnActionExecuting(ActionExecutingContext context)
        {
            var s = context.HttpContext.Request.ContentType;
            Console.WriteLine($"contentType is {s}");
            if (s == @"application/x-www-form-urlencoded")
            {
                var form = context.HttpContext.Request.Form;
                var formData = form.Count;
                foreach (var q in form)
                    Console.WriteLine($"formdata is {q.Value}");
            }
        }
    }
}
